using UnityEngine;

[CreateAssetMenu(fileName = "GridChild", menuName = "scriptable/GridChild", order = 0)]
public class GridChildScriptable : ScriptableObject
{
   public GameObject prefab;
   public Vector2 scale;
}

